import { NestFactory } from '@nestjs/core';
import { MainModule } from './main.module';
import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';

async function bootstrap() {
  const app = await NestFactory.create(MainModule);
  const port = process.env.PORT || process.env.PORT_DEV;

  app.use(cookieParser());
  app.use(cors());

  await app.listen(port);
}

bootstrap();
