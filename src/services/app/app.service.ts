import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { sendMethods } from 'src/utils/response';
import { ResetPasswordBlacklistService } from 'src/services/blacklist/resetPasswordBlacklist.service';
import { Logger } from '@nestjs/common';

@Injectable()
export class AppService {
  constructor(private readonly blackListPass: ResetPasswordBlacklistService) {}
  getHello(): any {
    return sendMethods({
      success: true,
      message: 'Servidor online!',
      data: null,
    });
  }

  @Cron('0 0 0 * * *')
  async handleRemoveBlackListsCron() {
    const removeDate = new Date();
    await this.blackListPass.removeAllToDate(removeDate);
    Logger.verbose(
      'Removido todos os hashs de reset de senha da black list anteriores a data ' +
        removeDate,
    );
  }
}
