import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { UserProps, UserUpdateProps } from 'src/interfaces/user.interface';
import { User } from 'src/classes/user/user.class';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<UserProps>,
  ) {}

  async create(data: UserProps): Promise<UserProps> {
    return await this.userRepository.save(data);
  }

  async update(id: string, data: UserUpdateProps): Promise<void> {
    await this.userRepository.update({ id }, data);
  }

  async findOne(condition: UserUpdateProps): Promise<UserProps> {
    return await this.userRepository.findOne(condition);
  }

  async findLogin(userName: string): Promise<UserProps> {
    return await this.userRepository
      .createQueryBuilder('users')
      .select('password, id')
      .where('email = :email', { email: userName })
      .orWhere('"userName" = :user', { user: userName })
      .getRawOne()
      .then((resp: UserProps) => resp);
  }

  async findCurrentPassword(id: string): Promise<UserProps> {
    return await this.userRepository
      .createQueryBuilder('users')
      .select('password')
      .where('id = :id', { id })
      .getRawOne()
      .then((resp: UserProps) => resp);
  }
}
