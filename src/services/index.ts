import { AppService } from './app/app.service';
import { ResetPasswordBlacklistService } from './blacklist/resetPasswordBlacklist.service';
import { UserService } from './user/user.service';

export default [
  AppService,
  UserService,
  ResetPasswordBlacklistService,
];
