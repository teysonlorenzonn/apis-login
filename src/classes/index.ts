import { User } from './user/user.class';
import { ResetPasswordBlackList } from './blacklist/resetPasswordBlacklist.class';

export default [
  User,
  ResetPasswordBlackList,
];
