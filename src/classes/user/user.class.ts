import { LevelUser, UserProps } from 'src/interfaces/user.interface';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('users')
export class User implements UserProps {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ unique: true })
  userName: string;

  @Column({ unique: true })
  cpfOrCnpj: string;

  @Column({ unique: true })
  email: string;

  @Column()
  phoneNumber: string;

  @Column()
  password: string;

  @Column({ type: 'varchar' })
  type: LevelUser;

  @Column({ type: 'varchar', nullable: true })
  tokenNotification: string | null;

  @CreateDateColumn()
  creationDate: Date;

  @UpdateDateColumn()
  lastModifyDate: Date;
}
