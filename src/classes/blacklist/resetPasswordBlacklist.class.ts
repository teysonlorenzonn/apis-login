import { BlackListProps } from 'src/interfaces/blacklist.interface';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
} from 'typeorm';

@Entity('resetPasswordBlackList')
export class ResetPasswordBlackList implements BlackListProps {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  hash: string;

  @CreateDateColumn()
  creationDate: Date;
}
