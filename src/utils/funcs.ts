import { isNil } from 'ramda';
import * as bcrypt from 'bcrypt';
import { sendMethods } from './response';

export const generateCrptyPass = async (pass: string) => {
  const salt = await bcrypt.genSalt();
  return await bcrypt.hash(pass, salt);
};

export const compareCrptyPass = async (pass: string, hash: string) => {
  return await bcrypt.compare(pass, hash);
};

export const normalizeInserts = (data: any) => {
  const normalize = {};
  for (const key in data) {
    if (!isNil(data[key])) {
      normalize[key] = data[key];
    }
  }
  return normalize;
};

export const genericError = 'Erro na comunicação com o banco de dados';

export const validJwt = async (
  auth: any,
  jwtService: any,
  hash?: string,
): Promise<any> => {
  let data = undefined;
  const cookie = !hash ? auth : hash;
  if (cookie) {
    data = await jwtService.verifyAsync(cookie);
  }
  return data;
};

export const convertHoursInMinutes = (hours: string) => {
  const interval = hours.split(':');

  const hour = Number(interval[0]);
  const min = Number(`0.${interval[1]}`);

  return Number(Number((hour + min) * 60).toFixed(2));
};

export const convertMinutesInHours = (minutes: number) => {
  const convert = Number((Number(minutes) / 60).toFixed(2));
  let hour = Math.trunc(convert);
  let min = convert - hour;

  if (min !== 0) {
    min = min % 60;
    if (min !== 0 && min > 60) {
      hour += 1 + min;
    } else {
      hour += min;
    }
  }

  let resp = String(hour.toFixed(2)).replace('.', ':');

  if (resp.length == 4) {
    resp = `0${resp}`;
  }
  return resp;
};

export const authUser = async (request: any, jwtService: any) => {
  let auth = request.headers['authorization'];
  if (!auth) {
    auth = request.cookies['token'];
  }
  const data = await validJwt(auth, jwtService);
  if (!data) {
    return sendMethods({
      message: 'Sessão expirada',
      success: false,
    });
  }
  return sendMethods({
    message: 'Usuário autorizado',
    success: true,
    data,
  });
};
