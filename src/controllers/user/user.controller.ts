import { Body, Controller, Get, Post, Req, Res, Put } from '@nestjs/common';
import { omit, isNil } from 'ramda';
import { JwtService } from '@nestjs/jwt';
import { Response, Request } from 'express';

import { Mailer } from 'src/services/email/mailer.service';
import { authUser } from 'src/utils/funcs';
import { send } from 'src/utils/response';
import { UserService } from 'src/services/user/user.service';
import { ResetPasswordBlacklistService } from 'src/services/blacklist/resetPasswordBlacklist.service';
import { generateCrptyPass, compareCrptyPass } from 'src/utils/funcs';
import { PasswordProps, UserProps } from 'src/interfaces/user.interface';
import { sendMethods } from 'src/utils/response';
@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly blackList: ResetPasswordBlacklistService,
    private jwtService: JwtService,
  ) {}

  @Get()
  async user(
    @Req() request: Request,
    @Res({ passthrough: true }) response: Response,
  ) {
    let resp: send;
    let statusCode = 200;

    try {
      const auth = await authUser(request, this.jwtService);
      if (auth.success) {
        const user = await this.userService.findOne({ id: auth.data.id });

        resp = sendMethods({
          message: 'Usuário consultado com sucesso',
          success: true,
          data: omit(['password'], user),
        });
      } else {
        resp = auth;
        statusCode = 401;
      }
    } catch (e) {
      statusCode = 403;
      resp = sendMethods({
        message: 'Sessão expirada',
        success: false,
      });
    }
    response.status(statusCode);
    return resp;
  }

  @Put()
  async updateUser(
    @Body() body: UserProps,
    @Res() response: Response,
    @Req() request: Request,
  ) {
    let resp: send;
    let statusCode = 202;
    try {
      const auth = await authUser(request, this.jwtService);
      if (auth.success) {
        if (!isNil(body.password)) {
          response.status(200);
          return sendMethods({
            message: 'Não é possível alterar a senha',
            success: false,
          });
        }

        if (body.userName) {
          body.userName = body.userName.toLowerCase().trim();
        }

        if (body.email) {
          body.email = body.email.toLowerCase().trim();
        }

        await this.userService.update(auth.data.id, body);

        resp = sendMethods({
          message: 'Usuário atualizado com sucesso',
          success: true,
        });
      } else {
        resp = auth;
        statusCode = 401;
      }
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode).json(resp);
  }

  @Post()
  async createUser(@Body() body: UserProps, @Res() response: Response) {
    let resp: send;
    let statusCode = 201;
    try {
      if (isNil(body.password)) {
        response.status(200).json(
          sendMethods({
            message: 'Usuário precisa de uma senha',
            success: false,
          }),
        );
        return;
      }
      const hashedPassword = await generateCrptyPass(body.password);
      const user = await this.userService.create({
        ...body,
        userName: body.userName.toLowerCase().trim(),
        email: body.email.toLowerCase().trim(),
        password: hashedPassword,
      });

      resp = sendMethods({
        message: 'Usuário criado com sucesso',
        success: true,
        data: omit(['password'], user),
      });
    } catch (e) {
      let msg = e;
      if (typeof msg === 'object') {
        if (e.detail?.includes('Key ("cpfOrCnpj")')) {
          msg = 'Cpf já está cadastrado para outra conta';
        } else if (e.detail?.includes('Key ("userName")')) {
          msg = 'Usuário já está cadastrado para outra conta';
        } else if (e.detail?.includes('Key (email)')) {
          msg = 'Email já está cadastrado para outra conta';
        }
      }
      statusCode = 400;
      resp = sendMethods({
        message: msg,
        success: false,
      });
    }
    response.status(statusCode).json(resp);
  }

  @Put('password')
  async resetPassword(
    @Body() body: PasswordProps,
    @Res() response: Response,
    @Req() request: Request,
  ) {
    let resp: send;
    let statusCode = 200;
    try {
      if (body.hash && (await this.blackList.findOne({ hash: body.hash }))) {
        response.status(200).json(
          sendMethods({
            message: 'Senha já foi atualizada com o hash atual',
            success: false,
          }),
        );
        return;
      }

      const auth = await authUser(request, this.jwtService);
      if (auth.success) {
        const { password } = await this.userService.findCurrentPassword(
          auth.data.id,
        );

        if (
          body.hash ||
          (await compareCrptyPass(body.currentPassword, password))
        ) {
          if (body.password === body.confirmPassword) {
            if (!(await compareCrptyPass(body.password, password))) {
              const hashedPassword = await generateCrptyPass(body.password);
              await this.userService.update(auth.data.id, {
                password: hashedPassword,
              });
              statusCode = 202;
              resp = sendMethods({
                message: 'Senha trocada com sucesso',
                success: true,
              });

              body.hash && (await this.blackList.create(body.hash));
            } else {
              resp = sendMethods({
                message: 'Não é possível aplicar a mesma senha',
                success: false,
              });
            }
          } else {
            resp = sendMethods({
              message: 'Confirmação de senha não é igual a senha',
              success: false,
            });
          }
        } else {
          resp = sendMethods({
            message: 'Senha atual não confere',
            success: false,
          });
        }
      } else {
        resp = auth;
        statusCode = 401;
      }
    } catch (e) {
      let msg = e;
      if (typeof msg === 'object') {
        if (e.detail?.includes('Key ("cpfOrCnpj")')) {
          msg = 'Cpf já está cadastrado para outra conta';
        } else if (e.detail?.includes('Key ("userName")')) {
          msg = 'Usuário já está cadastrado para outra conta';
        } else if (e.detail?.includes('Key (email)')) {
          msg = 'Email já está cadastrado para outra conta';
        }
      }
      statusCode = 400;
      resp = sendMethods({
        message: msg,
        success: false,
      });
    }
    response.status(statusCode).json(resp);
  }

  @Post('forgot')
  async forgotPassword(
    @Body('email') email: string,
    @Res() response: Response,
  ) {
    let resp: send;
    let statusCode = 200;
    try {
      const user = await this.userService.findOne({ email });
      if (!user) {
        response.status(401).json(
          sendMethods({
            message: 'Sessão expirada',
            success: false,
          }),
        );
        return;
      }

      const hash = await this.jwtService.signAsync({ id: user.id });

      const url = `http://seusite.com.br/usuario/trocar-senha?hash=${hash}`;

      const mailer = Mailer.useMailer();

      const success = await new Promise((resp, reject) => {
        mailer.verify((error) => {
          if (error) {
            reject(error);
          }
          const sendMail = {
            to: email,
            from: process.env.SEND_EMAIL,
            template: 'resetpass',
            subject: 'Esqueceu sua senha?',
            context: { url },
          };
          mailer.sendMail(sendMail, (error) => {
            if (error) {
              reject(error);
            }
            resp(null);
          });
        });
      })
        .then(() => true)
        .catch(() => false);

      if (success) {
        resp = sendMethods({
          message: 'Email enviado com sucesso',
          success: true,
        });
      } else {
        resp = sendMethods({
          message: 'Não foi possível enviar o email',
          success: false,
        });
      }
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode).json(resp);
  }

  @Post('login')
  async login(
    @Body('login') login: string,
    @Body('password') password: string,
    @Res({ passthrough: true }) response: Response,
  ) {
    let resp: send;
    let statusCode = 200;
    try {
      const user = await this.userService.findLogin(login.toLowerCase());
      if (!user) {
        response.status(401);
        return sendMethods({
          message: 'Sessão expirada',
          success: false,
        });
      }

      if (!(await compareCrptyPass(password, user.password))) {
        response.status(200);
        return sendMethods({
          message: 'Senha incorreta',
          success: false,
        });
      }

      const jwt = await this.jwtService.signAsync({ id: user.id });
      const getUser = await this.userService.findOne({ id: user.id });

      response.cookie('token', jwt, { httpOnly: true });

      resp = sendMethods({
        message: 'Usuário logado sucesso',
        success: true,
        data: { type: 'Bearer', token: jwt, user: omit(['password'], getUser) },
      });
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e.detail,
        success: false,
      });
    }
    response.status(statusCode);
    return resp;
  }

  @Post('logout')
  async logout(@Res({ passthrough: true }) response: Response) {
    let resp: send;
    let statusCode = 200;
    try {
      response.clearCookie('token');
      resp = sendMethods({
        message: 'Usuário deslogado sucesso',
        success: true,
      });
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode);
    return resp;
  }
}
