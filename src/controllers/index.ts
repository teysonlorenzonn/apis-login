import { UserController } from './user/user.controller';
import { AppController } from './app/app.controller';

export default [
  AppController,
  UserController,
];
