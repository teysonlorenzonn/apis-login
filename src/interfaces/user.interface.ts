export interface UserProps {
  id?: string;
  firstName: string;
  lastName: string;
  phoneNumber?: string;
  cpfOrCnpj: string;
  email: string;
  password: string;
  userName: string;
  lastModifyDate: Date;
  creationDate: Date;
  type: LevelUser;
  tokenNotification: string | null;
}

export interface PasswordProps {
  password: string;
  currentPassword: string;
  confirmPassword: string;
  hash?: string;
}

export interface UserUpdateProps {
  id?: string;
  firstName?: string;
  lastName?: string;
  phoneNumber?: string;
  email?: string;
  password?: string;
  userName?: string;
  cpfOrCnpj?: string;
  type?: LevelUser;
  tokenNotification?: string | null;
}

export type LevelUser = 'admin' | 'client' | 'visitant';
